import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { UserLogin } from "./entity/login";
import { User } from "./entity/user";

@Injectable()
export class LoginService {
    constructor(
        @InjectRepository(UserLogin)
        private loginRepository: Repository<UserLogin>,
        @InjectRepository(User)
        private userRepository: Repository<User>
    ) {}

    
}