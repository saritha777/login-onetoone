import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { UserLogin } from "./entity/login";
import { User } from "./entity/user";
import { LoginController } from "./login.controller";
import { LoginService } from "./login.service";

@Module({
    imports: [TypeOrmModule.forFeature([User, UserLogin])],
    controllers: [LoginController],
    providers: [LoginService]
})
export class LoginModule {}